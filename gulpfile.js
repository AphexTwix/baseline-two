var gulp                = require('gulp');
var sass                = require('gulp-ruby-sass');
var concat              = require('gulp-concat');
var cache               = require('gulp-cache');

// Styles
gulp.task('styles', function () {
    return sass('sass/**/*.scss', {
        loadPath: ['sass']
    }).pipe(gulp.dest('dist'));
});

// Watch
gulp.task('watch', function() {
    // Watch .scss files
    gulp.watch('sass/**/*.scss', ['styles']);
});

// Default
gulp.task('default', function() {
    gulp.start('styles', 'watch');
});